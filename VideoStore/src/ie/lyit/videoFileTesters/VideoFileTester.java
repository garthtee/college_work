package ie.lyit.videoFileTesters;

import ie.lyit.videos.Menu;
import ie.lyit.videos.VideoFileHandler;
import ie.lyit.videos.Video;

public class VideoFileTester {

	/**
	 * @author garthtee
	 * Date : 15/10/2015
	 * Class that tests the video file class 
	 */
	public static void main(String[] args) {
		
		
		
		
		
		
		// Create an object of VideoFileHandler
		VideoFileHandler videoFileHandler = new VideoFileHandler();
		
		// Send it an add() message once, to add one video  
//		videoFileHandler.add();
		
		// Write the ArrayList to file
//		videoFileHandler.writeRecordsToFile();
		
//		videoFileHandler.readRecordsFromFile();
		
//		videoFileHandler.list();

//		videoFileHandler.view(); // asks user to enter  a video number
		
		// Create a menu object
		Menu menuObj = new Menu();
		
		do{
			System.out.println("\n\n\t MENU \n");
			// Display menu
			menuObj.display();
			// Read option user enters
			menuObj.readOption();
			
			switch(menuObj.getOption()) {
				case 1:videoFileHandler.add();break;
				case 2:videoFileHandler.list();break;
				case 3:videoFileHandler.view();break;
				case 4:videoFileHandler.edit();break;
				case 5:videoFileHandler.delete();break;
				case 6:break;
				default: System.out.println("ERROR! Invalid option choosen.");
			}
			
		}while(menuObj.getOption() != 6);
	}

}
