package ie.lyit.videos;

import java.io.*;
import java.util.*;

public class Video implements Serializable{
   private int number;
	private String title;
	private String category;
	private double rentalCost;
	
	private static int nextNumber=1;
	
	public Video(){
	   number = 0;
		title = category = "";
		rentalCost = 0.0;
	}
	
	public Video(int number, String title, String category, double rentalCost){
	   this.number = nextNumber++;
		this.title = title;
		this.category = category;;
		this.rentalCost = rentalCost;
	}

	public void read(){
		Scanner kbInt = new Scanner(System.in);
		Scanner kbString = new Scanner(System.in);		
	   System.out.println("VIDEO DETAILS ==>");
	   System.out.print("NUMBER : ");number=kbInt.nextInt();
	   System.out.print("TITLE : ");title=kbString.nextLine();
	   System.out.print("CATEGORY : ");category=kbString.nextLine();
	   System.out.print("RENTAL COST : ");rentalCost=kbInt.nextDouble();
	}
	
	public void setNumber(int setNumberTo){
	   number=setNumberTo;
	}
	
	public int getNumber(){
	   return number;
	}
	
	public void setTitle(String setTitleTo){
	   title=setTitleTo;
	}
	
	public String getTitle(){
	   return title;
	}	

	public void setCategory(String setCategoryTo){
	   category=setCategoryTo;
	}
	
	public String getCategory(){
	   return category;
	}
	
	public void setRentalCost(double setRentalCostTo){
	   rentalCost=setRentalCostTo;
	}
	
	public double getRentalCost(){
	   return rentalCost;
	}	
	
 	public String toString(){
	   return(number + "\t" + title + "\t" + category + "\t" + rentalCost);
  	}

	public boolean equals(Video videoIn){
	   if((videoIn.number == this.number) && videoIn.title.equals(this.title) 
		     && videoIn.category.equals(this.category) && videoIn.rentalCost == this.rentalCost)
		   return true;
		else
		   return false;
	}
}
