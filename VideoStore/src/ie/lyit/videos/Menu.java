package ie.lyit.videos;

import java.util.Scanner;

public class Menu {
	
	private int option;
	
	/**
	 * Displaying the menu
	 */
	public void display() {
		System.out.println("\t1. Add");
		System.out.println("\t2. List");
		System.out.println("\t3. View");
		System.out.println("\t4. Edit");
		System.out.println("\t5. Delete");
		System.out.println("\t6. Quit");
	}
	
	/**
	 * Reads an integer from the keyboard 
	 * and stores that integer in the option
	 * variable 
	 */
	public void readOption(){
		System.out.println("\nEnter choosen option: ");
		Scanner keyboardIn = new Scanner(System.in);
		// Store entered option in class variable 
		this.option = keyboardIn.nextInt();
	}
	
	/**
	 * Returns the option variable
	 * @return
	 */
	public int getOption(){
		return this.option;
	}
	
}
