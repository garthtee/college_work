package ie.lyit.testers;

import ie.lyit.personel.*;

public class CustomerTester {

	public static void main(String[] args) {
		// Create a Customer object called cutomerA
		Customer customerA = new Customer();
		
		// Display customerA's details on screen
		System.out.println(customerA);
		
		// Set CustomerA's details
		customerA.setName(new Name("Mr", "Homer", "Simpson"));
		customerA.setPhoneNumber("0868863415");
		customerA.setEmailAddress("homer@gmail.com");
		
		// Display customerA's details on screen
		System.out.println(customerA);

		// Create a Customer object called cutomerA
		Customer customerB = new Customer("Miss", "Lisa", "Simpson", "0868863414", "lisa@gmail.com");
		
		// Display customerB's details on screen
		System.out.println(customerB);
		
		// Check if customerA equals customer B & print to screen
		System.out.println(customerA.equals(customerB));
		
		// Create another customer (C) the same as (B) so we know equals method works
		Customer customerC = new Customer("Miss", "Lisa", "Simpson", "0868863414", "lisa@gmail.com");		
		System.out.println(customerB.equals(customerC));
		
		// Check if customerB is female
		if(customerB.getName().isFemale())
			System.out.println(customerB.getName().getFirstName() + " is a female");
		else
			System.out.println(customerB.getName().getFirstName() + " is a male");
		
		// Check if customerA is female
		if(customerA.getName().isFemale())
			System.out.println(customerA.getName().getFirstName() + " is a female");
		else
			System.out.println(customerA.getName().getFirstName() + " is a male");
	}
}