package ie.lyit.testers;

import java.util.*;

import ie.lyit.personel.Date;
import ie.lyit.personel.Employee;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 30/09/2015
 * Description: Testing the Employee class
 * 
 */

public class EmployeeTester {

	public static void main(String[] args) {
		
		// Creating Employee objects
		Employee emp1 = new Employee("Mr", "Homer", "Simpson", 21, 7, 1970, 
				69999.00, 15, 9, 2012, "555-669-210");
		Employee emp2 = new Employee("Mrs", "Marge", "Simpson", 20, 8, 1971, 
				55000.00, 15, 9, 2012, "555-669-211");
		
		// Checking employee toString() and printing to screen
		System.out.println(emp1);
		System.out.println(emp2);
		
		// Checking getters && setters
		emp2.setDateOfBirth(new Date(2,2,2012));
		System.out.println("Emp2's new DOB = " + emp2.getDateOfBirth());
		emp2.setSalary(1000000.00);
		System.out.println("Emp2's new Salary = " + emp2.getSalary());
		emp2.setPhoneNumber("0868869452");
		System.out.println("Emp2's new Phone Number = " + emp2.getPhoneNumber());
		
		
		// Checking if two employees equals and printing to screen
		System.out.println("Is employee one equal to employee two? " + emp1.equals(emp2));
		
		emp2.setDateOfBirth(new Date(5,5,2005));
		System.out.println("Emp2's DOB = " +emp2.getDateOfBirth());
		
		
		// Printing emp1's employee numbers to screen
		System.out.println("Emp1's Employee number = " + emp1.getEmployeeNumber());
		
		// Creating an arrayList of object Employee and adding 4 employees to it
		ArrayList<Employee> employees = new ArrayList<>();
		
		Employee e1 = new Employee("Mr", "Bart", "Simpson", 21, 7, 1994, 
				60000.00, 15, 9, 2012, "555-111-210");
		employees.add(e1);
		
		Employee e2 = new Employee("Miss", "Lisa", "Simpson", 21, 7, 1995, 
				90000.00, 15, 9, 2012, "555-199-887");
		employees.add(e2);		
		
		Employee e3 = new Employee("Miss", "Maggie", "Simpson", 21, 7, 2005, 
				10000.00, 15, 9, 2012, "555-421-212");
		employees.add(e3);
		
		Employee e4 = new Employee("Mr", "Ned", "Flanders", 21, 7, 1975, 
				800000.00, 15, 9, 2012, "555-181-310");
		employees.add(e4);

		/*	Testing the increaseSalary  method	*/
		//////////////////////////////////////////
		
		System.out.println("Emp1's Salary BEFORE �250 increase = " + emp1.getSalary());
		emp1.increaseSalary();
		System.out.println("Emp1's Salary AFTER �250 increase = " + emp1.getSalary());
		
		System.out.println("Emp1's Salary BEFORE �250 increase = " + emp1.getSalary());
		emp1.increaseSalary();
		System.out.println("Emp1's Salary AFTER �250 increase = " + emp1.getSalary() + "\n(Salary Now over �70000 so not going to work)");
		
		/*	Testing the employeeSearch method	*/
		//////////////////////////////////////////
		
		System.out.println("Is e1 in the employees arrayList? " + employeeSearch(emp1, employees) +
				"\t(Should return false as emp1 is not in arrayList)"); 
		System.out.println("Is e1 in the employees arrayList? " + employeeSearch(e2, employees) + 
				"\t(Should return true as e2 is in arrayList)");

	}
	
	/**
	 * employeeSearch method checks to see if 
	 * the specified Employee object is stored inside the specified ArrayList
	 * @param employeeIn an Employee object
	 * @param employeeList	an ArrayList of type Employee
	 * @return boolean depending on whether or not the Employee parameter is inside
	 * the ArrayList parameter
	 */
	public static boolean employeeSearch(Employee employeeIn, ArrayList<Employee> employeeList) {
		
		boolean searchResult = false;
		for(Employee emp : employeeList) {
			if(emp.equals(employeeIn)){
				searchResult = true;
			}
		}
		
		return searchResult;
	}
}