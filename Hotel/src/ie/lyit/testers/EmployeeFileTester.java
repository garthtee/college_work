package ie.lyit.testers;

import ie.lyit.personel.Employee;
import ie.lyit.personel.EmployeeFileHandler;

public class EmployeeFileTester {

	
	
	public static void main(String[] args) {

		EmployeeFileHandler empFileHandler = new EmployeeFileHandler();
		
		empFileHandler.readRecordsFromFile();
		
		
		empFileHandler.addEmployee();
		
		empFileHandler.writeRecordsToFile();
		empFileHandler.readRecordsFromFile();		
		
		// List all employees
		empFileHandler.listEmployees();
		
	}

}
