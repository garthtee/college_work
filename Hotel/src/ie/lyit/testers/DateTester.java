package ie.lyit.testers;

import javax.swing.JOptionPane;

import ie.lyit.personel.Date;

public class DateTester {
	
	public static void main(String[] args) {

		boolean correctInput = false;
		
		do {
			try {
				
				Date d1 = new Date();
				JOptionPane.showMessageDialog(null, "Please enter a date...");
				String dayAsString = JOptionPane.showInputDialog(null, "Enter the day");
				int dayAsNumber = Integer.parseInt(dayAsString);			
				String monthAsString = JOptionPane.showInputDialog(null, "Enter the month");
				int monthAsNumber = Integer.parseInt(monthAsString);
				String yearAsString = JOptionPane.showInputDialog(null, "Enter the year");
				int yearAsNumber = Integer.parseInt(yearAsString);
				d1.setDay(dayAsNumber);
				d1.setMonth(monthAsNumber);
				d1.setYear(yearAsNumber);
				System.out.println(d1);
				correctInput=true;
				
			} catch (IllegalArgumentException exp){
				System.out.println(exp.getMessage());
			}catch (Exception exp){ // This is a catch all block in-case something isn't caught! 
				System.out.println(exp.getMessage());
			}
		}while(!correctInput);
		
	}
}