package ie.lyit.testers;

import java.util.ArrayList;

import ie.lyit.personel.Name;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 17/09/2015
 * Description: Testing the name class
 *
 */

public class NameTester {

	public static void main(String[] args) {
		
		// Constructing names
		Name person1 = new Name("Miss","Joe","Lafferty");
		Name person2 = new Name("Mr","Garth","Toland");
		Name person3 = new Name("Mr","Garth","Toland");
		Name person4 = new Name("Mr","Garth","Toland");
		Name person5 = new Name("Mr","Garth","Toland");
		
		// Display on screen
		//System.out.println("person one: " + person1);
		
		System.out.println(person1.getTitle());
		System.out.println("person two: " + person2);
		
		// Changing name1's first name to Paula
		person1.setFirstName("Paula");
		// Changing name2's surname to Jelly
		person2.setSurname("Jelly");
		
		// Display on screen
		System.out.println("person one: " + person1);
		System.out.println("person two: " + person2);
		
		// test the equals method and printing on screen
		System.out.println("Does person one equal person two? " + person1.equals(person2));
		
		// Testing the isFemale method and printing on screen
		System.out.println("Is person one a female? " + person1.isFemale());
		System.out.println("Is person two a female? " + person2.isFemale());
		
		// test the equals method and printing on screen
		System.out.println("Does person two equal person three? " + person2.equals(person3));

		// test the equals method and printing on screen
		System.out.println("Does person four equal person five? " + person4.equals(person5));
		
		
		// Creating an array list for storing the names 
		
		ArrayList<Name> names = new ArrayList<Name>();
		names.add(person1);
		names.add(person2);
		names.add(person3);
		names.add(person4);
		names.add(person5);
		names.trimToSize(); // trims the capacity of names to be names current size
		
		// Print all of the list
		for (Name name : names)
		{
			System.out.println(name);
		}
		
		/*	NameSearch method	*/
		//////////////////////////
		
		System.out.println("Is the Name Garth in the names ArrayList? " + nameSearch("Garth", names));
		System.out.println("Is the Name John in the names ArrayList? " + nameSearch("John", names));
		
	}
	
	/**
	 * Searches through list parameter for the nameIn parameter
	 * @param nameToFind name as a string
	 * @param listOfNames a ArrayList of names
	 * @return true if name is contained in the list
	 */
	public static boolean nameSearch(String nameToFind, ArrayList<Name> listOfNames){
		boolean result = false;
		for (Name name : listOfNames)
		{
			if(name.getFirstName().toLowerCase().equals(nameToFind.toLowerCase())){
				result = true;
			}
		}
		return result;
	}

}