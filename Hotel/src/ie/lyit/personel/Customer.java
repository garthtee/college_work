package ie.lyit.personel;

import ie.lyit.personel.Person;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 24/09/2015
 * Description: Models a Customer
 *
 */

// INHERITANCE - Customer IS-A Person - Customer has a Name and a phoneNumber and emailAddress
public class Customer extends Person {
	
	private String emailAddress;
	
	/**
	 * Default Constructor
	 * Called when object is created like this
	 * 	==> Customer cObj = new Customer();
	 */
	public Customer(){
		super(); // NOTE: Don't need to call super()
		emailAddress = null;
	}
	
	/**
	 * Initialization Constructor
	 * Called when object is created like this
	 * Customer cObj = new Customer("Mr","Garth","Toland","0868863416","tolandgarth@gmail.com");
	 * @param title	name title 
	 * @param firstName of a Name
	 * @param surname of a Name
	 * @param phoneNumber of a Person
	 * @param emailAddress of a Customer
	 */
	public Customer(String title, String firstName, String surname, String phoneNumber, String emailAddress){
		super(title, firstName, surname, phoneNumber);
		this.emailAddress = emailAddress; 
	}
	
	/**
	 * OVERRIDING the Person toString() methods
	 * Calling Person's toString(0 and adding additional information
	 */
	@Override
	public String toString() {
		return "Customer {" + super.toString() +", Email: " + emailAddress + "}";
	}
	
	/**
	 * Overriding the Person equals() method
	 * ==> Called when comparing an object with another object
	 * @param customer object of the Customer class
	 * @return boolean depending on whether or not the two objects match 
	 */
	public boolean equals(Customer customer) {
		if(super.equals(customer) &&
					emailAddress.equals(customer.getEmailAddress())) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/*	Getters && Setters	*/
	//////////////////////////
	
	/**
	 * Gets the Customers email address
	 * @return String object representing a Customers email address
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * Sets the Customers email address
	 * @param emailAddress String value representing a Customers email address
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}