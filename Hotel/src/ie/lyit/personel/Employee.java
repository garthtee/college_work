package ie.lyit.personel;

import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 30/09/2015
 * Description: Models an Employee
 * 
 */

public class Employee extends Person implements Serializable {
	
	private Date dateOfBirth;
	private double salary;
	private Date startDate;
	private int employeeNumber;
	private Random rn = new Random();;
	private static int maxEmployees = 10000;
	
	/**
	 * Default Constructor
	 * Constructs a new object of Employee and 
	 * initializes the values to null except EMPLOYEE_NO 
	 * which is one more than the previous
	 */
	public Employee() {
		dateOfBirth = new Date();
		salary = 0.0;
		startDate = new Date();
		employeeNumber = rn.nextInt(maxEmployees);
	}
	
	/**
	 * Initialization Constructor
	 * @param title of a Name
	 * @param firstName of a Name
	 * @param surname of a Name
	 * @param birthDay of a Date
	 * @param birthMonth of a Date
	 * @param birthYear of a Date
	 * @param salary of an employee
	 * @param startDay of a Date
	 * @param startMonth of a Date
	 * @param startYear of a Date
	 * @param phoneNumber of a person
	 */
	public Employee(String title, String firstName, String surname, int birthDay, int birthMonth, int birthYear, 
			Double salary, int startDay, int startMonth, int startYear, String phoneNumber) {
		super(title, firstName, surname, phoneNumber);
		this.dateOfBirth = new Date(birthDay, birthMonth, birthYear);
		this.salary = salary;
		this.startDate = new Date(startDay, startMonth, startYear);
		employeeNumber = rn.nextInt(maxEmployees);
	}
	
	/**
	 * Getting employee details from the user
	 */
	public void read() {
			//	Reading in name and phone number	//
		try{
			
			String titleInput = JOptionPane.showInputDialog("Enter title: ");
			String firstNameInput = JOptionPane.showInputDialog("Enter first name: ");
			String surnameInput = JOptionPane.showInputDialog("Enter surname: ");
			super.name = new Name(titleInput, firstNameInput, surnameInput);
	//		super.setName(new Name(titleInput, firstNameInput, surnameInput));
			super.phoneNumber = JOptionPane.showInputDialog("Enter phone number: ");
		} catch(NullPointerException e){
			System.out.println("ERROR, Please fill out all fields.");
		}
		//	Reading in date of birth	//
		try{
			int birthDayInput = Integer.valueOf(JOptionPane.showInputDialog("Enter birth day: "));
			int birthMonthInput = Integer.valueOf(JOptionPane.showInputDialog("Enter birth month: "));
			int birthYearInput = Integer.valueOf(JOptionPane.showInputDialog("Enter birth year: "));
			this.dateOfBirth = new Date(birthDayInput, birthMonthInput, birthYearInput);
		} catch(NullPointerException e) {
			System.out.println("ERROR. Date value not entered. Please try again.");
		} catch(Exception e){
			System.out.println("ERROR. Incorrect date format");
		}
		//	Reading in start date	//
		try{
			int startDayInput = Integer.valueOf(JOptionPane.showInputDialog("Enter start day: "));
			int startMonthInput = Integer.valueOf(JOptionPane.showInputDialog("Enter start month: "));
			int startYearInput = Integer.valueOf(JOptionPane.showInputDialog("Enter start year: "));
			this.startDate = new Date(startDayInput, startMonthInput, startYearInput);
		} catch(Exception e){
			System.out.println("Incorrect date format");
		}
		//	Reading in salary	//
		try{
			this.salary = Double.valueOf(JOptionPane.showInputDialog("Enter salary: "));
		} catch(NumberFormatException e) {
			System.out.println("ERROR. Salary value not entered / entered correctly. Please try again.");
		}
	}
	
	/**
	 * OVERRIDING the Employee toString() methods
	 * Calling Person's toString() and adding additional information
	 * from the Employee class
	 */
	@Override
	public String toString(){
		return "Employee " + this.getEmployeeNumber() + "  {" + super.toString() + ", \nDOB: " + this.dateOfBirth + ", Salary: " + this.salary + ", Start Date: "  + this.startDate + "}";
	}
	
	/**
	 * Overriding the Employee equals() method
	 * ==> Called when comparing an object with another object
	 * @param employee
	 * @return
	 */
	public boolean equals(Employee employee){
		if(super.equals(employee) && this.dateOfBirth.equals(employee.getDateOfBirth())
				&& this.phoneNumber.equals(employee.getPhoneNumber())
				&& this.employeeNumber == employee.getEmployeeNumber()){
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Increments the employee's salary by 250
	 * if the salary is less than 70000.00 
	 */
	public void increaseSalary(){
		if(this.salary <= 70000.00){
			this.salary += 250.0; // adding 250 to the salary
		}
	}

	/*	Getters && Setters	*/
	//////////////////////////
	
	/**
	 * Gets the Date of birth of an Employee
	 * @return a Date object representing an Employee's DOB
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets the Date Of Birth of the Employee
	 * @param dateOfBirth takes in a Date object
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Gets the current salary
	 * @return double value representing the Employee's salary
	 */
	public double getSalary() {
		return salary;
	}

	/**
	 * Sets the salary of the Employee
	 * @param salary takes in a double value overwriting the current salary
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}

	/**
	 * Gets the start date of the Employee
	 * @return a Date object representing the employee's start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Sets the date the employee started 
	 * @param startDate takes in a Date object
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the Employee's unique number
	 * @return integer value representing the employee number
	 */
	public int getEmployeeNumber() {
		return employeeNumber;
	}

	/**
	 * Sets the Employee's unique number
	 * @param employeeNumber
	 */
	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}
	
	
	
	
}