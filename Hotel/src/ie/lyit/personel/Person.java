package ie.lyit.personel;

import java.io.Serializable;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 24/09/2015
 * Description: Models a Person
 *
 */

public abstract class Person implements Serializable {

	protected Name name; 	// COMPOSITION - Person HAS-A name
	protected String phoneNumber;
	
	/**
	 * Default Constructor 
	 * Called when object is created like this
	 * ==> Person pObj = new Person();
	 *  NOTE - This won't work because Person is abstract
	 */
	public Person() {
		name = new Name();
		phoneNumber = null;
	}

	/**
	 * Initialization constructor
	 * Called when object would have been created like this (NOT possible now)
	 * 		==> Person pObj = new Person("Mr", "Garth", "Toland", "0868863416");
	 * @param title
	 * @param firstName
	 * @param surname
	 * @param phoneNumber
	 */
	public Person(String title, String firstName, String surname, String phoneNumber) {
		this.name = new Name(title, firstName, surname); // Calls Name initialization constructor
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * toString() Method
	 *  ==> Calls Name's toString() to display Name details and 
	 *  then displays phone number
	 */
	public String toString(){
		return name + ", Phone No: " + phoneNumber;
	}
	
	/**
	 * equals() Method
	 * ==> Called when comparing and object with another object,
	 *  e.g. - if(p1.equals(p2))
	 *  ==> Calls Call's Name's equals() to compare name to Person's name
	 * @param person takes in phoneNumber so it can be compared
	 * @return boolean depending on whether or not the two objects match 
	 */
	public boolean equals(Person person) {
		if(name.equals(person.name) && phoneNumber.equals(person.phoneNumber)){
			return true;
		}
		else{
			return false;
		}
	}
	
	/*	Getters and Setters	*/
	//////////////////////////
	
	/**
	 * Gets Name
	 * @return name in String form 
	 */
	public Name getName() {
		return name;
	}

	/**
	 * Sets the Name
	 * @param name takes in a Name object
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * Gets the Phone Number of the Person
	 * @return String representing the Person's Phone No.
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	/**
	 * Sets the Phone Number of the Person
	 * @param phoneNumber takes in a String representing the Person's Phone No.
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}	
}