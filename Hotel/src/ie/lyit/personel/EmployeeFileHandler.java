package ie.lyit.personel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class EmployeeFileHandler {

	private ArrayList<Employee> employees;
	
	public EmployeeFileHandler() {
		employees = new ArrayList<>();
	}
	
	public void addEmployee() {
		Employee employee = new Employee();
		
		// read employee details from user
		employee.read();
		
		if(employee.getDateOfBirth().getDay() == 0 ||
				employee.getDateOfBirth().getMonth() == 0 ||
						employee.getDateOfBirth().getYear() == 0 ||
								employee.getStartDate().getDay() == 0 ||			// If anything wasn't entered
									employee.getStartDate().getMonth() == 0 ||		// don't continue.
										employee.getStartDate().getYear() == 0 ||
											employee.getPhoneNumber() == null ||
												employee.getName().getTitle() == null ||
													employee.getName().getFirstName() == null ||
														employee.getName().getSurname() == null) {
			
			System.out.println("You must fill out all fields.");
		} else {
		
			// Checking that the employee ID is not the same as previous employees
			if(!employees.isEmpty()){		
				for(Employee tmpEmployee : employees){
					if(tmpEmployee.getEmployeeNumber() == employee.getEmployeeNumber()) {
						int empNo = employee.getEmployeeNumber();
						int empListSize = employees.size();
						int total = empNo + empListSize;
						employee.setEmployeeNumber(total + 1);
					}
				}	
			}
			
			// Add new employee to list
			employees.add(employee);
		}
	}
	
	public void writeRecordsToFile() {
		try{
			
			FileOutputStream fos = new FileOutputStream("Employees.bin");
			
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(employees);
			
			oos.close();
			
			
		} catch(FileNotFoundException e) {
			System.out.println("Cannot create file to store employees.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void readRecordsFromFile() {
		try{
			
			FileInputStream fis = new FileInputStream("Employees.bin");
			
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			employees = (ArrayList<Employee>) ois.readObject();
			
			ois.close();
			
		} catch(FileNotFoundException fNFE) {
			System.out.println("Cannot create file to read");
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void listEmployees() {
		
//		System.out.print("" +"\t" +"" +"\t" +"" +"\t" +"" +"\n");
		
		// Check to see if array isn't empty
		if(employees.size() < 1){
			System.out.println("No employees added.");
		} else {
			for(Employee employee : employees){
				System.out.println(employee);
			}
		}
	}
	
	public void view(){
		
		int employeeNoIn = Integer.valueOf(JOptionPane.showInputDialog("Enter employee number: "));
		
		// Check to see if array isn't empty
		if(employees.size() < 1){
			System.out.println("No employees added.");
		} else {
			for(Employee employee : employees){
				if(employeeNoIn == employee.getEmployeeNumber())
					System.out.println(employee);
				else
					System.out.println("No employee found!");					
			}
		}
		
	}
	
	public void deleteEmployee() {
		
		int employeeToDelete = Integer.valueOf(JOptionPane.showInputDialog("Enter employee number to delete: "));
		
		// Check to see if array isn't empty
		if(employees.size() < 1){
			System.out.println("No employees added.");
		} else {	
			for(Employee tmpEmployee : employees) {
				if(tmpEmployee.getEmployeeNumber() == employeeToDelete) {
					// Remove employee from list
					employees.remove(tmpEmployee);
					
					System.out.println(tmpEmployee.getName() +" deleted!");
				}
			}
		}
	}
	
	public void editEmployee() {
		
		int employeeToEdit = Integer.valueOf(JOptionPane.showInputDialog("Enter employee number to edit: "));
		
		// Check to see if array isn't empty
		if(employees.size() < 1){
			System.out.println("No employees added.");
		} else {
			for(Employee tmpEmployee : employees) {
				
				if(tmpEmployee.getEmployeeNumber() == employeeToEdit) {
					// Display employee to be edited
					System.out.println("You choose to edit " +tmpEmployee.getName());
					// Get it's index
					int index = employees.indexOf(tmpEmployee);
					// Read in a new employee
					tmpEmployee.read();
					// Reset the object in employee list
					employees.set(index, tmpEmployee);
					break;
				}
			}
		}
	}
	
}// End class
