package ie.lyit.personel;

import java.io.Serializable;

/**
 * 
 * @author L00106273 - Garth Toland
 * @version 1.0
 * Date: 17/09/2015
 * Description: Models a name
 *
 */

public class Name implements Serializable {
	// Instance variables
	private String title;
	private String firstName;
	private String surname;

	/* Constructors to initialize the Instance Variables */
	///////////////////////////////////////////////////////
	
	/**
	 * ==> Called when a name object is created as follows -
	 * 	   Name name = new Name();
	 */
	public Name() {
		this.title = null;
		this.firstName = null;		// Can also be done this way..
		this.surname = null;		//title=firstName=surname = null;
	}
	
	/**
	 * ==> Called when a name object is created as follows -
	 * Name name = new Name("Mr","Garth","Toland");
	 * @param t title of name
	 * @param fN firstName of name
	 * @param sN surname of name
	 */
	public Name(String t, String fN, String sN) {
		this.title = t;
		this.firstName = fN;
		this.surname = sN;
	}
	
	public Name(String fN, String sN) {
		this.firstName = fN;
		this.surname = sN;
	}
	
	/**
	 * ==> Called when a string of the class is used, e.g. -
	 * System.out.print(name);
	 * or System.out.print(name.toString());
	 * @return the name in full
	 */
	public String toString() {
		return "Name: " + title + " " + firstName + " " + surname;
	}
	
	/**
	 * ==> Called when comparing one object with another
	 * @param name takes in a name
	 * @return returns true of false depending on comparison
	 */
	public boolean equals(Name name) {
		if(title.equals(name.getTitle()) && 
			firstName.equals(name.getFirstName()) && 
			surname.equals(name.getSurname()))
			return true;
		else
			return false;
	}
	
	/*	Variable Getters and setters	*/
	/*	Called when setting or getting part of an object	*/
	
	/**
	 * Gets name title
	 * @return title of name
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets name title
	 * @param title of name
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets first name
	 * @return firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets first name
	 * @param firstName of name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Gets surname
	 * @return surname of name
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Sets surname of name
	 * @param takes in a surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}	
	
	/**
	 * Checks if title is female
	 * @return true if female & false if male
	 */
	public boolean isFemale() {
		if(title.toLowerCase().equals("miss") || 
			title.toLowerCase().equals("mrs") || 
			title.toLowerCase().equals("ms"))
			return true;
		else
			return false;
	}
}