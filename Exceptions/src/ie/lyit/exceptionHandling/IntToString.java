package ie.lyit.exceptionHandling;

import javax.swing.JOptionPane;

public class IntToString {

	public static void main(String[] args) {
		
		boolean correctInput=false; // A correct input would be in Integer value
		
		do {
		
			try {
				String numberAsString = JOptionPane.showInputDialog(null, "Enter an integer");
				
				// Attempt to convert the string to an Integer
				int number = Integer.parseInt(numberAsString); // this parseInt method causes the exception as it looks for an Integer
				System.out.print("Conversion was sucessful!\n" +"The integer entered is " +number);
				correctInput=true;
	
			} catch(NumberFormatException exp) { // catching exact exception
				System.out.println("Cannot convert to an Integer. " +exp.getMessage());
			} catch (Exception exp) {
				System.out.println("An error occurred. " +exp.getMessage());
			}
		
		}while(!correctInput);
	}

}